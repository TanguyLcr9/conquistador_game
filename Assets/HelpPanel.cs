﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour
{
    public Sprite[] images;
        public Image image;
    public GameObject panel;

    private int index =0;

    public void NextImage(bool invert)
    {
        if (invert)
        {
            index--;
            if (index == -1)
                index = images.Length - 1;
        }
        else
        {
            index++;
            if (index == images.Length)
                index = 0;
        }
        image.sprite = images[index];
    }

    public void Return()
    {
        panel.SetActive(false);

    }

    public void OpenHelp()
    {
        panel.SetActive(true);
    }
}
