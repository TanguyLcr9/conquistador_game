﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flee : MonoBehaviour
{
    public NavMeshAgent agent;
    public GameObject corps;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        agent.speed = 10;
        agent.destination = new Vector2(21, 0);

        if (transform.position.x > 20)
        {
            Destroy(gameObject);

        }

    }
}
