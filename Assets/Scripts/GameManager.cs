﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public int[] ennemieOfWave;//Customisable en fonction du niveau
    public static GameManager instance;
    public bool demo = false;
    public GameObject buildSlotUI, buildingUI, buildSlotSelected, gameOverUI, winUI, HUD, buttonsHUD, securityPanel, securityPanel_2;
    public List<GameObject> productPanel;
    public List<Button> buttonsPanel;
    public TextMeshProUGUI nameBuild; //éléments d'UI affichant le type de batiment 
    public TextMeshProUGUI[] productText, costText;
    public TextMeshProUGUI[] buildText;
    public GameObject[] typeBuildDisplay;
    public AudioMixerSnapshot[] snapshots;
    public AudioMixer audioMixer;
    public AudioClip buttonSound;


    [SerializeField]
    private TextMeshProUGUI popNum, primNum, supNum; // composants texte affichant respectivement la population, primaries, supplies
    private int wave;
    private float timer;
    private bool gameOver = false;

    public Slider enerSlider;

    public Menu menuGO;
    public int population, primaires, supplies, energy, maxEnergy = 10;
    public List<BuildSlot> buildSlot = new List<BuildSlot>();
    public List<Building> buildingPrefab = new List<Building>();
    public List<Building> building = new List<Building>();
    public List<Enemy> enemies = new List<Enemy>();
    public List<Warrior> warriors = new List<Warrior>();
    public List<GameObject> sbirePrefabs = new List<GameObject>();
    public List<EnemySpawner> enemySpawners = new List<EnemySpawner>();

    private bool winterIsHere = false;


    // Start is called before the first frame update
    void Awake()
    {

        buildSlotUI.SetActive(false);
        buildingUI.SetActive(false);
        gameOverUI.SetActive(false);
        if (demo)
        {
            HUD.SetActive(false);
        }
        else
        {
            HUD.SetActive(true);
            DontDestroyOnLoad(this);

        }
        menuGO = GameObject.FindWithTag("Menu").GetComponent<Menu>();
    }
    private void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        instance = this;

        enerSlider.maxValue = maxEnergy;
        energy = maxEnergy;
    }

    void UpdateVariables()
    {

        popNum.text = "" + population;
        primNum.text = "" + primaires;
        supNum.text = "" + supplies;
        enerSlider.value = energy;

        timer = 0;
    }

    public void UpdateButtons()
    {
        foreach (Button buttons in buttonsPanel)
        {
            buttons.interactable = true;
        }
        if (primaires < (50 + (warriors.Count) * 5) || supplies < (50 + (warriors.Count) * 15))
        {
            buttonsPanel[3].interactable = false;// guerriers
        }

        if (primaires < (75 + (warriors.Count) * 5) || supplies < (25 + (warriors.Count) * 15))
        {
            buttonsPanel[5].interactable = false;// archers
        }

        if (primaires < (15 + (warriors.Count) * 5) || supplies < (35 + (warriors.Count) * 5))
        {
            buttonsPanel[6].interactable = false;// population
        }

        if (primaires < (150 + (warriors.Count) * 15) || supplies < (200 + (warriors.Count) * 15) || energy < 2)
        {
            buttonsPanel[7].interactable = false;//greatWarrior
        }



        foreach (Building buildings in building)
        {

            switch (buildings.type)
            {
                case 0:
                    buttonsPanel[10].interactable = false;
                    //  Debug.Log(buildings.type);
                    break;
                case 1:
                    buttonsPanel[9].interactable = false;
                    //  Debug.Log(buildings.type);
                    break;
                case 2:
                    buttonsPanel[11].interactable = false;
                    // Debug.Log(buildings.type);
                    break;
            }
        }


        if (primaires < 200 || energy < 1)//reparer
        {
            buttonsPanel[8].interactable = false;
        }

        if (primaires < (150 + 50 * (building.Count - 1)) || supplies < (150 + 50 * (building.Count - 1)) || energy < 2)
        {

            buttonsPanel[9].interactable = false;
            buttonsPanel[10].interactable = false;
            buttonsPanel[11].interactable = false;
            buttonsPanel[12].interactable = false;
        }


    }

    private void Update()
    {
        if (demo)
        {
            foreach (BuildSlot slot in buildSlot)
            {
                slot.col.enabled = false;
            }
            foreach (Building builds in building)
            {
                builds.col.enabled = false;
            }

        }
        if ((energy <= 0) && !winterIsHere)
        {
            buildSlotUI.SetActive(false);
            buildingUI.SetActive(false);
            foreach (BuildSlot slot in buildSlot)
            {
                slot.col.enabled = false;
            }
            foreach (Building builds in building)
            {
                builds.col.enabled = false;
            }

            StartCoroutine(enemySpawners[0].SpawnEnemies(ennemieOfWave[wave++]));
            winterIsHere = true;

            float[] weights = { 0, 1 };
            if (!demo)
                audioMixer.TransitionToSnapshots(snapshots, weights, 1f);



        } else if (winterIsHere && enemies.Count == 0)
        {
            foreach (BuildSlot slot in buildSlot)
            {
                slot.col.enabled = true;
            }
            foreach (Building builds in building)
            {
                builds.col.enabled = true;
            }
            winterIsHere = false;
            energy = maxEnergy;
            float[] weights = { 1, 0 };
            if (!demo)
                audioMixer.TransitionToSnapshots(snapshots, weights, 1f);
        }

        timer += Time.deltaTime;

        if (wave == ennemieOfWave.Length && enemies.Count == 0 && !winterIsHere && !gameOver)
        {
            winUI.SetActive(true);
        }

        if (timer > 0.5f)
        {
            UpdateVariables();
        }
    }

    //public void ProductPrimairies(int product) {
    //    primaires += product;
    //}

    //public void ProductSupplies (int product)
    //{
    //    supplies += product;
    //}

    //public void ProductWarrior(int cost)
    //{
    //    Instantiate(sbirePrefabs[0], buildSlotSelected.transform.position, Quaternion.identity);

    //}

    //public void RepareBuilding(int cost)
    //{
    //    buildSlotSelected.GetComponent<Building>().Repare();
    //    buildingUI.SetActive(false);
    //    foreach (BuildSlot slot in buildSlot)
    //    {
    //        slot.col.enabled = true;
    //    }
    //    foreach (Building builds in building)
    //    {
    //        builds.col.enabled = true;
    //    }
    //    primaires -= cost;
    //}

    //public void EnergyCost(int cost)
    //{
    //    energy -= cost;
    //}

    public void ActionButton(int i)
    {
        switch (i)
        {
            case 0://Boutton prduction casser de la pierre

                primaires += (50 + 5 * (warriors.Count));
                energy -= 1;

                break;
            case 1://Boutton prduction cueillette

                supplies += (60 + 5 * (warriors.Count));
                energy -= 1;

                break;
            case 2://Boutton prduction chasse

                supplies += (100 + 10 * (warriors.Count));
                primaires += (40 + 3 * (warriors.Count));
                energy -= 1;

                break;
            case 3://Boutton prduction guerriers

                Instantiate(sbirePrefabs[0], buildSlotSelected.transform.position + Vector3.down * 2, Quaternion.identity);
                primaires -= (50 + (warriors.Count) * 5);
                supplies -= (50 + (warriors.Count) * 15);
                energy -= 1;

                break;
            case 4://Boutton prduction bois

                primaires += (140 + 10 * (warriors.Count));
                energy -= 1;

                break;
            case 5://Boutton prduction archer

                Instantiate(sbirePrefabs[0], buildSlotSelected.transform.position + Vector3.down * 2, Quaternion.identity);
                primaires -= (75 + (warriors.Count) * 5);
                supplies -= (25 + (warriors.Count) * 15);
                energy -= 1;

                break;
            case 6://production d'habitants

                Instantiate(sbirePrefabs[2], buildSlotSelected.transform.position + Vector3.down * 2, Quaternion.identity);
                Instantiate(sbirePrefabs[2], buildSlotSelected.transform.position + Vector3.down * 2 + Vector3.left, Quaternion.identity);

                primaires -= (15 + (warriors.Count) * 5);
                supplies -= (35 + (warriors.Count) * 5);
                energy -= 1;
                break;
            case 7://Boutton prduction greatwarrior

                Instantiate(sbirePrefabs[3], buildSlotSelected.transform.position + Vector3.down, Quaternion.identity);
                primaires -= (15 + (warriors.Count) * 5);
                supplies -= (35 + (warriors.Count) * 5);
                energy -= 2;

                break;
            case 8://Boutton prduction reparation

                buildSlotSelected.GetComponent<Building>().Repare();
                buildingUI.SetActive(false);
                foreach (BuildSlot slot in buildSlot)
                {
                    slot.col.enabled = true;
                }
                foreach (Building builds in building)
                {
                    builds.col.enabled = true;
                }
                primaires -= 200;
                energy -= 1;

                break;
            case 9://creation tipi

                energy -= 2;

                break;
            case 10:
                supplies += 50 + warriors.Count * 5;
                energy -= 1;
                break;
        }
        StartCoroutine(SoundButton());

        StartCoroutine(WaitForUpdateText());

        UpdateButtons();
    }

    public IEnumerator WaitForUpdateText()
    {
        yield return new WaitForSeconds(0.2f);

                buildSlotSelected.GetComponent<Building>().UpdateText();
    }



    public void GameOver()
    {
        gameOver = true;
        gameOverUI.SetActive(true);
        buildingUI.SetActive(false);
        buildSlotUI.SetActive(false);
        HUD.SetActive(false);
    }

    public void ReturnMenu(int increment)
    {
        if (SceneManager.GetActiveScene().buildIndex - 1 == menuGO.level)
        {
            menuGO.level += increment;
        }
        instance = null;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        menuGO.mainMenu.SetActive(true);
        Destroy(this.gameObject);
        float[] weights = { 1, 0 };
            audioMixer.TransitionToSnapshots(snapshots, weights, 1f);
        StartCoroutine(SoundButton());
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        float[] weights = { 1, 0 };
        audioMixer.TransitionToSnapshots(snapshots, weights, 1f);
        instance = null;
        Destroy(this.gameObject);
        StartCoroutine(SoundButton());
    }

    public void GoBackToMenu(int i)
    {
        switch (i)
        {
            case 0://
                securityPanel.SetActive(false);
                securityPanel_2.SetActive(false);
                buttonsHUD.SetActive(true);
                Camera.main.GetComponent<CameraDragMove>().enabled = true;

                break;
            case 1:
                securityPanel.SetActive(true);
                buttonsHUD.SetActive(false);
                Camera.main.GetComponent<CameraDragMove>().enabled = false;
                break;
            case 2:
                securityPanel_2.SetActive(true);
                buttonsHUD.SetActive(false);
                Camera.main.GetComponent<CameraDragMove>().enabled = false;
                break;
            case 3:
                securityPanel.SetActive(false);
                buttonsHUD.SetActive(false);
                instance = null;
                SceneManager.LoadScene(0, LoadSceneMode.Single);
                menuGO.mainMenu.SetActive(true);
                Destroy(this.gameObject);
                break;
        }
        Debug.Log("000FEV");
        StartCoroutine(SoundButton());
    }



    public void QuitGame()
    {
        StartCoroutine(SoundButton());
        Application.Quit();

    }

    public IEnumerator SoundButton()
    {
        AudioSource audioButton = gameObject.AddComponent<AudioSource>();
        audioButton.clip = buttonSound;
        audioButton.volume = 0.5f;
        audioButton.Play();
        yield return new WaitForSeconds(audioButton.clip.length);
        Destroy(audioButton);
}
}
