﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

abstract public class Sbire : MonoBehaviour
{
    public Transform target;
    public GameManager gameManager;
    public GameObject boomParticles;
    [Range(0, 10)]
    public  float speed;
    public bool onAttack = false;
    public int hp = 4;
    public NavMeshAgent agent;
    public SpriteRenderer[] spr;
    public GameObject corps;
    public AudioClip[] scream;
    

    virtual public void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        //agent. = ;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        StartCoroutine("DoCheck");
    }

    virtual public void Update()
    {
        spr[0].sortingOrder = -(int)(transform.position.y * 5);
        spr[1].sortingOrder = -(int)(transform.position.y * 5)+1;
 for (int i = 2; i < spr.Length-1; i++) { 
            spr[i].sortingOrder = -(int)(transform.position.y * 5) - 1;
    }
        spr[spr.Length-1].sortingOrder = -(int)(transform.position.y * 5) - 2;

        if (target != null && transform.position.x - target.position.x > 0)
        {
            corps.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            corps.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        Movement();
        if (target == null) { return; }
        if (!onAttack &&Vector2.Distance(target.transform.position, transform.position)<2f)
        {
            StartCoroutine("DoAttack"); 
        }
    }

    public bool CheckingProximityTarget(Transform other, float distance)
    {// vérifie la proximité par rapport à une distance
        if (Vector2.Distance(other.position, transform.position) < distance)
        {
            return true;
        }
        return false;
    }

    virtual public void Movement()
    {

    }

    virtual public IEnumerator DoCheck()//sélectionne une nouvelle cible si elle se trouve à proximité
    {
        return null;
    }

    virtual public IEnumerator DoAttack()//effectue une attaque sur la cible
    {

        return null;
    }

    virtual public void TakeDamage(int damage)
    {
        hp -= damage;

        if (hp <= 0)
        {
            gameManager.warriors.Remove(gameObject.GetComponent<Warrior>());
            Destroy(gameObject);
        }

    }

    public IEnumerator SoundScream()
    {
        AudioSource sbireSound = gameObject.AddComponent<AudioSource>();
        int ran = Random.Range(0, scream.Length);
        sbireSound.clip = scream[ran];
        sbireSound.volume = 0.03f;
        sbireSound.Play();
        yield return new WaitForSeconds(sbireSound.clip.length);
        Destroy(sbireSound);
    }
}
