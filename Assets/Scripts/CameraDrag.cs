﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrag : MonoBehaviour
{

    private float dragSpeed = 20;
    public Vector3 dragOrigin, pos, move;
    public Vector3 cameraPosition;

    private float maxHeight = 10f, minHeight = -10f, maxWidth = 10f, minWidth = -10f;

    private void Start()
    {
        dragSpeed = 20;
    }

    private void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Camera.main.ScreenToViewportPoint(Input.mousePosition)+new Vector3(-0.5f,-0.5f,0f);
        }

        if (!Input.GetMouseButton(0))
        {
            cameraPosition = transform.position;
            return;
        }

        pos = Camera.main.ScreenToViewportPoint(Input.mousePosition) + new Vector3(-0.5f, -0.5f, 0f )- dragOrigin;
        move = -pos * dragSpeed;

        if (transform.position.y >= maxHeight)
        {
          
        }
        else if (transform.position.y <= minHeight)
        {

        }

        if (transform.position.x >= maxWidth)
        {

        }
        else if (transform.position.x <= minWidth)
        {

        }
        //  transform.Translate( move , Space.World);
        transform.position = new Vector3(move.x, move.y, -10f)+cameraPosition;




    }

    //void FixedUpdate()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        dragOrigin = Input.mousePosition;
    //        return;
    //    }

    //    if (!Input.GetMouseButton(0)) return;

    //    Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
    //    Vector3 move = new Vector3(pos.x * dragSpeed, pos.y * dragSpeed,0f);

    //    transform.Translate(move, Space.World);
    //}

    /*public Vector3 initPosition;
    public Vector2 offset;

    void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            initPosition = new Vector3(Input.mousePosition.x - transform.position.x, Input.mousePosition.y - transform.position.y, -10f);
        }
        if (Input.GetButton("Fire1"))
        {
            transform.position = (new Vector3((offset.x + Input.mousePosition.x - initPosition.x) * 0.01f, (offset.y + Input.mousePosition.y - initPosition.y) * 0.01f, 10f)) * -1f;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            offset.x = transform.position.x;
            offset.y = transform.position.y;
        }
    }*/

}
