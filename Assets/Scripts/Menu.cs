﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject mainMenu, levelSelector, options, credits;

    public Button[] levelButtons;

    public int level;

    public static Menu instance;
    public AudioClip buttonSound;

    private void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void OpenLevelSelector()
    {
        for(int i=0; i < levelButtons.Length; i++)
        {
                levelButtons[i].interactable = i <= level ? true : false; //pour rendre accessible seulement les nouveaux niveaux les uns aprèes les autres
           
        }
        mainMenu.SetActive(false);
        options.SetActive(false);
        levelSelector.SetActive(true);
        DontDestroyOnLoad(this.gameObject);
        StartCoroutine(SoundButton());
    }

    public void PlayGame(int lvl)
    {
        levelSelector.SetActive(false);
        SceneManager.LoadScene(lvl,LoadSceneMode.Single);
        StartCoroutine(SoundButton());
    }

    public void ReturnMainMenu()
    {
        mainMenu.SetActive(true);
        levelSelector.SetActive(false);
        options.SetActive(false);
        credits.SetActive(false);
        StartCoroutine(SoundButton());

    }

    public void OpenOptions()
    {
        mainMenu.SetActive(false);
        levelSelector.SetActive(false);
        options.SetActive(true);
        StartCoroutine(SoundButton());
    }

    public void OpenCredits()
    {
        mainMenu.SetActive(false);
        levelSelector.SetActive(false);
        options.SetActive(false);
        credits.SetActive(true);
        StartCoroutine(SoundButton());
    }

    public void QuitGame()
    {
        //Systeme de sauvegarde à prévoir
        Application.Quit();
    }

    public IEnumerator SoundButton()
    {
        AudioSource audioButton = gameObject.AddComponent<AudioSource>();
        audioButton.clip = buttonSound;
        audioButton.volume = 0.5f;
        audioButton.Play();
        yield return new WaitForSeconds(audioButton.clip.length);
        Destroy(audioButton);
    }
}
