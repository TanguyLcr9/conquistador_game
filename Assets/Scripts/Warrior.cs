﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Warrior : Sbire
{
    public LifeBehaviour life;
    public int type;

    public override void Start()
    {
        base.Start();
        gameManager.warriors.Add(this);
        gameManager.population++;
        transform.SetParent(GameObject.Find("Warriors").transform);

    }



    override public void Movement()
    {

        if (target == null) { life.enabled = true; }
        else
        {

            life.enabled = false;
            if (type == 0)
            {
                agent.destination = target.position;
            }
            else
            {
                agent.destination = new Vector3(-20f, 0f, 0f);
            }
        }



        //if(target!=null)
        //{
        //    life.enabled = false;
        //    Vector2 dir = target.position - transform.position;
        //    transform.Translate(dir.normalized * speed * Time.deltaTime);
        //}
        //else
        //{}
        //   life.enabled = true;

    }

    public override IEnumerator DoAttack()
    {

        onAttack = true;
        yield return new WaitForSeconds(.8f);

        if (target != null)
        {
            Instantiate(boomParticles, target.transform.position, Quaternion.identity);
            StartCoroutine(SoundScream());
            // Debug.Log(target.gameObject.name);
            target.GetComponent<Enemy>().target = transform;
            target.GetComponent<Enemy>().TakeDamage(1);
        }

        onAttack = false;
    }

    override public IEnumerator DoCheck()
    {
       
            yield return new WaitForSeconds(Random.Range(0, .4f));
        for (; ; )//boucle infinie
        {
            foreach (Enemy enemie in gameManager.enemies)//pour tout les batiments construits
            {
                if (enemie.enabled && CheckingProximityTarget(enemie.transform, 20f))
                {
                    if (target == null)
                    {
                        target = enemie.transform;
                        agent.enabled = true;

                    }
                    else if (CheckingProximityTarget(enemie.transform, Vector2.Distance(target.position, enemie.transform.position)))
                    {
                        target = enemie.transform;
                        agent.enabled = true;

                    }
                }
            }
            yield return new WaitForSeconds(.4f);//recommence toute les .4secondes
        }
    }

    private void OnDestroy()
    {
        gameManager.warriors.Remove(this);
        gameManager.population--;

        for (int i = 0; i < gameManager.enemies.Count; i++)
        {
            if (gameManager.enemies[i].target = transform)
            {
                gameManager.enemies[i].StopCoroutine("DoAttack");
                gameManager.enemies[i].target = null;
                gameManager.enemies[i].onAttack = false;

            }
        }
    }

    private void OnDrawGizmos()
    {
        if (target != null)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, target.position);
        }

    }
}
