﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LifeBehaviour : MonoBehaviour
{
    public GameManager gameManager;
    [HideInInspector]
    public Transform target;
    public Collider2D col;
    public GameObject corps;
    public float speed;
    public NavMeshAgent agent;

    public void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        col = gameObject.GetComponent<Collider2D>();
        col.isTrigger = true;
        target = gameManager.building[Random.Range(0, gameManager.building.Count)].transform;
        speed = Random.Range(1f, 1.5f);
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;

    }

    private void Update()
    {
        Movement();

        if (target != null && transform.position.x - target.position.x > 0)
        {
            corps.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            corps.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    void Movement()
    {

        if (target != null)
        {
            if (agent.enabled == true)
                agent.destination = target.position;
            if (Vector2.Distance(transform.position, target.position) < 1.5f)
            {
                corps.SetActive(false);

                col.enabled = false;
                //agent.enabled = false;
                StartCoroutine("StayInPlace");
                target = null;

            }
        }
        else
        {

        }


    }

    private void OnEnable()
    {
        if (agent != null)
        {
            agent.speed = speed;
        }
    }

    private void OnDisable()
    {
        agent.speed = GetComponent<Warrior>().speed;
    }


    IEnumerator StayInPlace()
    {

        yield return new WaitForSeconds(Random.Range(.4f, 6f));
        int ran = Random.Range(0, gameManager.building.Count);
        while (target == gameManager.building[ran].transform  && !gameManager.building[ran].isDestroy)//pour éviter au pnj de retourner au même endroit
        {
            ran = Random.Range(0, gameManager.building.Count);
        }
        target = gameManager.building[ran].transform;
        corps.SetActive(true);
        col.enabled = true;
        // agent.enabled = true;

    }
}
