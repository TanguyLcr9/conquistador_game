﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDragMove : MonoBehaviour
{

    public bool demo;
    [Range(5,20)]
    public int maxX=5, maxY=5;

    private Vector3 Origin;
    private Vector3 Diference;
    private Camera cam;
    private bool Drag = false;
    void Start()
    {
        cam = GetComponent<Camera>();
    }
    void LateUpdate()
    {
        if (demo) { return; }
        if (Input.GetMouseButton(0))
        {
            Diference = (Camera.main.ScreenToWorldPoint(Input.mousePosition)) - Camera.main.transform.position;
            if (Drag == false)
            {
                Drag = true;
                Origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        else
        {
            Drag = false;
        }

        if (Drag == true)
        {

            float xDepl=0, yDepl=0;

            if (Origin.x - Diference.x >= -maxX && Origin.x - Diference.x <= maxX)
            {
                xDepl = Origin.x - Diference.x;
            }
            else if(Origin.x - Diference.x < -maxX)
            {
                xDepl = -maxX;
            }
            else if (Origin.x - Diference.x > maxX)
            {
                xDepl = maxX;
            }

            if (Origin.y - Diference.y >= -maxY && Origin.y - Diference.y <= maxY)
            {
                yDepl = Origin.y - Diference.y;
            }
            else if(Origin.y - Diference.y < -maxY)
            {
                yDepl = -maxY;
            }else if(Origin.y - Diference.y > maxY)
            {
                yDepl = maxY;
            }

            Camera.main.transform.position = new Vector3(xDepl, yDepl,-10);
        }



    }
    private void Update()
    {
        if (demo) { return; }
        if (cam.orthographicSize >= 2f && cam.orthographicSize <= 30f)
        {
            cam.orthographicSize += Input.GetAxis("Mouse ScrollWheel");
        }

        if (cam.orthographicSize < 2f)
        {
            cam.orthographicSize = 2f;
        }
        else if (cam.orthographicSize > 15f)
        {
            cam.orthographicSize = 15f;
        }
    }
}
