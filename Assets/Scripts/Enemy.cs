﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Sbire
{
    Vector3 endingPoint;

    public override void Start()
    {
        base.Start();
        gameManager.enemies.Add(this);
        if (GameObject.Find("EndingPathEnemies") != null)
        {
            endingPoint = GameObject.Find("EndingPathEnemies").transform.position;
        }
        else { endingPoint = new Vector3(-30f, 0, 0); }
        transform.SetParent(GameObject.Find("Enemies").transform);
    }

    override public IEnumerator DoCheck()//sélectionne une nouvelle cible si elle se trouve à proximité
    {
         yield return new WaitForSeconds(Random.Range(0, .4f));
        for (; ; )//boucle infinie
        {

            foreach (Building builds in gameManager.building)//pour tout les batiments construits
            {
                if (!builds.isDestroy)
                {
                    if (CheckingProximityTarget(builds.transform, 100f))
                    {
                        if (target == null)
                        {
                            target = builds.transform;
                            agent.enabled = true;

                        }
                        else if (CheckingProximityTarget(builds.transform, Vector2.Distance(target.position, transform.position)))
                        {
                            target = builds.transform;
                            agent.enabled = true;

                        }
                    }
                }
            }
            foreach (Warrior warrior in gameManager.warriors)//pour tout les warriors présents
            {
                if (warrior.enabled)
                {
                    if (CheckingProximityTarget(warrior.transform, 8f))
                    {
                        if (target == null)
                        {
                            target = warrior.transform;
                            agent.enabled = true;
                        }
                        else if (CheckingProximityTarget(warrior.transform, Vector2.Distance(target.position, transform.position)))
                        {
                            target = warrior.transform;
                            agent.enabled = true;
                        }
                    }
                }
            }
          

            yield return new WaitForSeconds(.4f);//recommence toute les .4secondes
        }
    }

    override public void Movement()
    {
        agent.destination = target == null ? endingPoint : target.position;

        if (transform.position.x < -20f) {
            gameManager.enemies.Remove(gameObject.GetComponent<Enemy>());
            Destroy(this.gameObject);
        }

        //if (target == null)//if there is no target
        //{
        //    if (gameManager.building.Count != 0)
        //    {

        //        transform.Translate(Vector2.left * speed * Time.deltaTime);//step forward
        //    }


        //}
        //else //if (coll Vector2.Distance(target.position, transform.position) > 1)
        //{
        //    Vector2 dir = target.position - transform.position;
        //    transform.Translate(dir.normalized * speed * Time.deltaTime);
        //}
    }

    private void OnDrawGizmos()
    {
        if (target != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, target.position);
        }

    }

    override public IEnumerator DoAttack()//effectue une attaque sur la cible
    {

        onAttack = true;
        yield return new WaitForSeconds(.8f);
        if (target != null)
        {
            Instantiate(boomParticles, target.transform.position, Quaternion.identity);
            StartCoroutine(SoundScream());
            //   Debug.Log(target.gameObject.name);

            if (target.CompareTag("Warrior"))
            {
                target.GetComponent<Warrior>().TakeDamage(1);
            }
            else
            {
                target.GetComponent<Building>().TakeDamage(1);
            }

        }
       
        onAttack = false;
    }

    override public void TakeDamage(int damage)
    {
        hp -= damage;

        if (hp <= 0)
        {

            enabled = false;
            gameObject.GetComponent<Flee>().enabled = true;

            corps.transform.eulerAngles += new Vector3(0, 180, 0);

            gameManager.enemies.Remove(this);

            for (int i = 0; i < gameManager.warriors.Count; i++)
            {
                if (gameManager.warriors[i].target = transform)
                {

                    gameManager.warriors[i].target = null;
                    gameManager.warriors[i].StopCoroutine("DoAttack");
                    gameManager.warriors[i].onAttack = false;

                }
            }
        }

    }
}
