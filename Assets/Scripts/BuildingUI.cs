﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingUI : MonoBehaviour
{
    private GameManager gameManager;


    void Start()
    {
        gameManager = GetComponent<GameManager>();
    }

    public void ReturnOn() {//reactive les colliders et désactive l'UI 

        foreach (BuildSlot slot in gameManager.buildSlot)
        {
            slot.col.enabled = true;
        }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = true;
        }
        gameManager.buildSlotUI.SetActive(false);
        gameManager.buildingUI.SetActive(false);
        gameManager.buildSlotSelected = null;
        StartCoroutine(gameManager.SoundButton());
    }

    public void CreateBuilding(int type)
    {
        Building instance = Instantiate(gameManager.buildingPrefab[type], gameManager.buildSlotSelected.transform.position, Quaternion.identity);
        instance.transform.SetParent(GameObject.Find("Board").transform);
        gameManager.buildSlotSelected.SetActive(false);
        for (int i=0; i < 2; i++)
        {
           Instantiate(gameManager.sbirePrefabs[2], instance.transform.position - Vector3.up, Quaternion.identity);
        }
        gameManager.primaires -= 150 + 50*(gameManager.building.Count-1);
        gameManager.supplies -= 150 + 50 * (gameManager.building.Count - 1);
        gameManager.energy -= 2;
        StartCoroutine(gameManager.SoundButton());
        ReturnOn();
    }

}
