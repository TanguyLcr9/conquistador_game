﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int index = 1;
    public  GameObject sbire;
    public bool demo=false;

    private static float range = 5f;

    private void Start()
    {
        sbire = GameObject.FindWithTag("GameManager").GetComponent<GameManager>().sbirePrefabs[index];
        GameObject.Find("GameManager").GetComponent<GameManager>().enemySpawners.Add(this);
        if (demo)
        {
            StartCoroutine(SpawnEnemies(100));
        }

        // StartCoroutine(SpawnEnemies(10));
    }

    public IEnumerator SpawnEnemies(int nbOfEnemies)
    {
        for(int i = 0; i < nbOfEnemies; i++)
        {
            Instantiate(sbire, transform.position + new Vector3(0f,Random.Range(-range, range)), Quaternion.identity);
            if (demo)
            {
                yield return new WaitForSeconds(1f);
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position + new Vector3(0, -range), transform.position + new Vector3(0, range));
    }
}
