﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Building : BuildSlot
{
    [Range(0, 4)]
    public int type;
    private int hp = 4;
    private static string[] nameBuild = { "Tipi des chAsseurs", "Tipi des bUcherons", "Tipi de guerrier", "Tipi d'hAbitAnts", "Tipi du chef" };
    public bool isDestroy;
    private Color colorBase;
    public Sprite tipiDestroy;
    public Sprite baseTipiSprite;
   

    override public void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        col = GetComponent<Collider2D>();
        gameManager.building.Add(this);
        spr = GetComponentInChildren<SpriteRenderer>();
        colorBase = spr.color;
        baseTipiSprite = spr.sprite;
    }

    override public void ShowUI()
    {
        for (int i = 0; i < gameManager.typeBuildDisplay.Length; i++) {
            gameManager.typeBuildDisplay[i].SetActive(i == type ? true : false);
                }


        gameManager.nameBuild.text = nameBuild[type];
        
        gameManager.buildingUI.SetActive(true);
        foreach (BuildSlot slot in gameManager.buildSlot)
        {
            slot.col.enabled = false;
        }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = false;
        }
        onClicked = false;
    }

    public void TakeDamage(int attackPower) {
        hp -= attackPower;
        if (hp <= 0)
        {
            IsDestroying();
        }
    }

    public void UpdateText()
    {
        foreach (GameObject Panels in gameManager.productPanel)
        {
            Panels.SetActive(false);
        }
        if (isDestroy)
        {
            gameManager.productPanel[8].SetActive(true);

        }
        else
            switch (type)
            {
                case 0: // chasseru
                    gameManager.productPanel[2].SetActive(true);//viande
                    gameManager.productText[2].text = "PRODUCTION :\nNOURRITURE + " + (100 + 10 * (gameManager.warriors.Count)) + "\nRESSOURCES PREMIERES + " + (40 + 3 * (gameManager.warriors.Count));
                    gameManager.costText[2].text = "energie - 1";

                    gameManager.productPanel[5].SetActive(true);//archer
                    gameManager.productText[5].text = "FORMATION :\nARCHER + 1";
                    gameManager.costText[5].text = "energie - 1\nRESSOURCES PREMIERES - " + (75 + (gameManager.warriors.Count) * 5) + "\nNOURRITURE - " + (25 + (gameManager.warriors.Count) * 15);
                    break;

                case 1: //bucheron
                    gameManager.productPanel[1].SetActive(true);//nourriture
                    gameManager.productText[1].text = "PRODUCTION :\nNOURRITURE + " + (60 + 5 * (gameManager.warriors.Count));
                    gameManager.costText[1].text = "energie - 1";

                    gameManager.productPanel[4].SetActive(true);//bois
                    gameManager.productText[4].text = "PRODUCTION :\nRESSOURCES PREMIERES + " + (140 + 10 * (gameManager.warriors.Count));
                    gameManager.costText[4].text = "energie - 1";
                    break;

                case 2: //guerriers
                    gameManager.productPanel[3].SetActive(true);//guerrier
                    gameManager.productText[3].text = "PRODUCTION :\nGUERRIER + 1";
                    gameManager.costText[3].text = "energie - 1\nRESSOURCES PREMIERES - " + (50 + (gameManager.warriors.Count) * 5) + "\nNOURRITURE - " + (50 + (gameManager.warriors.Count) * 15);
                    break;

                case 3: // habitat
                    gameManager.productPanel[6].SetActive(true);//habitant
                    gameManager.productText[6].text = "PRODUCTION :\nHABITANTS + 1";
                    gameManager.costText[6].text = "energie - 1\nRESSOURCES PREMIERES - " + (15 + (gameManager.warriors.Count) * 5) + "\nNOURRITURE - " + (35 + (gameManager.warriors.Count) * 5);
                    break;

                case 4: // chef
                    gameManager.productPanel[0].SetActive(true);//casser la pierre
                    gameManager.productText[0].text = "PRODUCTION :\nRESSOURCES PREMIERES + " + (50 + 5 * (gameManager.warriors.Count));
                    gameManager.costText[0].text = "energie - 1";

                    gameManager.productPanel[9].SetActive(true);//casser la pierre
                    gameManager.productText[9].text = "PRODUCTION :\nNOURRITURE + " + (50 + 5 * (gameManager.warriors.Count));
                    gameManager.costText[9].text = "energie - 1";


                    gameManager.productPanel[7].SetActive(true);//grand chef
                    gameManager.productText[7].text = "PRODUCTION :\nGRAND GUERRIER + 1";
                    gameManager.costText[7].text = "energie - 2\nRESSOURCES PREMIERES - " + (150 + (gameManager.warriors.Count) * 15) + "\nNOURRITURE - " + (200 + (gameManager.warriors.Count) * 15);
                    break;

            }
    }

    override public void OnMouseDown()
    {
        UpdateText();

        base.OnMouseDown();
    }

    public void IsDestroying()
    {
        spr.sprite = tipiDestroy;
        isDestroy = true;
        if(type == 4)
        {
            gameManager.GameOver();
        }

        for (int i = 0; i < gameManager.enemies.Count; i++)
        {
            if (gameManager.enemies[i].target == transform && gameManager.enemies[i].target != null)
            {
                gameManager.enemies[i].target = null;
                gameManager.enemies[i].StopCoroutine("DoAttack");
                gameManager.enemies[i].onAttack = false;
            }
        }
    }

    public void Repare()
    {
        isDestroy = false;
        spr.sprite = baseTipiSprite;
    }
}
