﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BuildSlot : MonoBehaviour
{
    public GameManager gameManager;

    protected SpriteRenderer spr;
    [HideInInspector]
    public Collider2D col;
    protected bool onClicked;
    public GameObject bulle;
    public static bool firstClick = false;

    private string[] buildName = { "des chAsseurs", "des bUcherons", "des guerriers", "d'hAbitAnts", "du chef" };

    virtual public void Start() {
        spr = GetComponentInChildren<SpriteRenderer>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        col = GetComponent<Collider2D>();
        gameManager.buildSlot.Add(this);
        StartCoroutine(WaitFirstClick());
    }

    void Update()
    {
        spr.sortingOrder = -(int)(transform.position.y*5);
        if (onClicked) //!Input.GetButtonDown("Fire1"))
        {
            ShowUI();
        }
    }

    virtual public void ShowUI()
    {
        int index = 0;
        foreach (TextMeshProUGUI bText in gameManager.buildText)
        {
            bText.text = "TIPI "+ buildName[index]+"\n BESOINS : "+ (150 + 50 * (gameManager.building.Count - 1))  + " RESSOURCES PRIMAIRES & "+ (150 + 50 * (gameManager.building.Count - 1))+ " DE NOURRITURE";
            index++;
        }
        gameManager.buildSlotUI.SetActive(true);
        foreach (BuildSlot slot in gameManager.buildSlot)
        {
            slot.col.enabled = false;
        }
        foreach (Building builds in gameManager.building)
        {
            builds.col.enabled = false;
        }

        onClicked = false;
    }

    virtual public void OnMouseDown() {//Active L'UI buildSlot et désactive tous les colliders des Slots
        onClicked = true;
        firstClick = true;
        gameManager.buildSlotSelected = this.gameObject;
        gameManager.UpdateButtons();
    }

    IEnumerator WaitFirstClick()
    {

        yield return new WaitUntil(() => firstClick);
        {
            bulle.SetActive(false);
        }
    }

    
}
